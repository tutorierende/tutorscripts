#!/bin/sh

if [ $# -lt 2 ] || ([ ! -f 'grades.csv' ] && [ ! -f 'grades.xls' ]); then
    printf "Usage: %s <repository name> <branch>\n" $(basename $0) >&2
    printf "Supply a repository name and branch name\n" >&2
    printf "Execute within the directory containing 'grades.csv'\n" >&2
    exit 1
fi

repo_name="$1"
branch="$2"

for user in *; do
    if [ -d "$user" ]; then
        cd "$user"

        current_dir=$(basename "$PWD")

        zedat_username=`echo $current_dir | sed 's/.*(\([a-z0-9]*\))$/\1/'`

        if [ ! -d npvp-exercises ]; then
            git clone "git@git.imp.fu-berlin.de:$zedat_username/$repo_name"
        fi

        git -C "$repo_name" checkout "$branch"
        cd ..
    fi
done
