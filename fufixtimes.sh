#! /bin/sh

if [ ! -f 'grades.csv' ] && [ ! -f 'grades.xls' ]; then
    printf "Execute within the directory containing 'grades.csv'\n" >&2
    exit 1
fi

find . -exec touch {} \;
