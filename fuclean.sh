#! /bin/sh

if [ ! -f 'grades.csv' ] && [ ! -f 'grades.xls' ]; then
    printf "Execute within the directory containing 'grades.csv'\n" >&2
    exit 1
fi

# Delete empty comments.txts so we can more easily delete empty directories.
# Otherwise they would still contain this one empty file.
for pattern in "comments.txt" "feedbackText.html" "*_submissionText.html"; do
    find . -type f -name "$pattern" -empty -delete;
done

# Delete all empty directories in the directory containing grades.csv
find . -type d -empty -delete

# Re-create the (now deleted) feedback attachment directories and comments.txts for the student directories that are left.
for dir in *; do
	if [ -d "${dir}" ]; then
		mkdir "${dir}/Feedback Attachment(s)"
		touch "${dir}/comments.txt"
	fi
done
