#! /bin/sh

if [ $# -lt 1 ] || ([ ! -f 'grades.csv' ] && [ ! -f 'grades.xls' ]); then
    printf "Usage: %s <pattern>\n" $(basename $0) >&2
    printf "Supply a pattern matching files to be copied, e.g. '*.pdf'\n" >&2
    printf "Execute within the directory containing 'grades.csv'\n" >&2
    exit 1
fi
patterns=$@

for pattern in $patterns; do
    find \
        -wholename "*/Submission attachment(s)/$pattern" \
        -execdir cp {} '../Feedback Attachment(s)' \;
done
